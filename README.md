# Simple PHP FPM + Nginx with Docker


This project is example of simple implementation PHP FPM with Nginx Web server in Docker, we separate configuration for deployment in Docker and source code

## Deployment in Docker

If you want deploy this application in docker, just execute file build.sh in the root directory, make sure you have install docker engine in your machine.

## Explanation of Directory and Files

This is important directory will be used in this project: 
> `server/etc/` : files conf for php-fpm and nginx\
> `server/entrypoint.sh` : files executed when container created\
> `Dockerfile` : file definition for build image in this project\
> `build.sh` : simple executable script to running build image and run container

