FROM php:7.2-fpm-alpine

RUN apk add bash
RUN apk add nginx

COPY server/etc/nginx /etc/nginx
COPY server/etc/php-fpm /usr/local/etc
COPY server/entrypoint.sh /etc/entrypoint.sh

# Copy all exclude server folder
COPY . /usr/share/nginx/html 

WORKDIR /usr/share/nginx/html

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN composer install

EXPOSE 80
EXPOSE 443

# Softkill with SIGTERM not forced like SIGKILL
STOPSIGNAL SIGTERM

ENTRYPOINT ["sh", "/etc/entrypoint.sh"]